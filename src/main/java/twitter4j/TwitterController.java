package twitter4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import twitter4j.*;

import java.util.List;

@RestController
public class TwitterController {

    @Autowired
    private TwitterService twitterService;

    @GetMapping("/timeline")
    public List<String> getTimeline() {
        return twitterService.getTimeline();
    }

    @GetMapping("/trends")
    public ResponseList<Location> getTrends() throws TwitterException {
        return twitterService.getTrends();
    }
}