package twitter4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import twitter4j.*;
import twitter4j.api.TrendsResources;

import java.util.ArrayList;
import java.util.List;

@Service
public class TwitterService {
    public Twitter twitter = TwitterFactory.getSingleton();

    public List<String> getTimeline() {
        List<String> tweets = new ArrayList<>();
        try {
            ResponseList<Status> homeTimeline = twitter.getHomeTimeline();
            for (Status status : homeTimeline) {
                tweets.add(status.getText());
            }
        } catch (TwitterException e) {
            throw new RuntimeException(e);
        }
        return tweets;
    }

    public ResponseList<Location> getTrends() throws TwitterException{
        return twitter.trends().getAvailableTrends();
    }

}
